import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
const Home = () => {
  return (
    <div>
      <Jumbotron fluid>
        <Container fluid>
          <h1 className="display-3">Corona Explorer</h1>
          <p className="lead">Choose a map to show in the navigation above ^</p>
        </Container>
      </Jumbotron>
    </div>
  );
};

export default Home;
