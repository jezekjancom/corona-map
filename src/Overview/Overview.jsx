import React, { Component } from 'react';
import { readRemoteFile } from 'react-papaparse';
import '../App.css';
import 'mapbox-gl/dist/mapbox-gl.css';
import MapBase from '../MapBase';
import Calculation from '../Calculation';
import Api from '../Api';

import ReactGA from 'react-ga';
ReactGA.initialize('UA-111511982-7');
ReactGA.pageview(window.location.pathname + window.location.search);

class App extends Component {
  state = {
    dataDaily: [],
    dataTimeseries: [],
    selectedCountries: [],
    date: Calculation.actualDate,
  };

  componentDidMount() {
    Api.fetchDataDaily(this.state.date);
    this.fetchDataTimeseries();
  }

  updateDataDailyHandler = result => {
    const dataDaily = result.data;
    this.setState({ dataDaily });
  };

  setCountriesHandler = entries => {
    // let uniqueCountries = [...new Set(entries)];
    var result = Calculation.removeDuplicates(entries, 'Country/Region');
    this.setState({ countries: result });
  };

  updateDataTimeseriesHandler = result => {
    const dataTimeseries = result.data;
    this.setState({ dataTimeseries });
    this.setCountriesHandler(dataTimeseries);
  };

  fetchDataTimeseries = () => {
    readRemoteFile(process.env.REACT_APP_FILE_TIMESERIES, {
      header: true,
      download: true,
      complete: this.updateDataTimeseriesHandler,
    });
  };

  fetchDataDaily = (date = `${this.state.date}`) => {
    let FILE_URL = process.env.REACT_APP_FILE_DAILY.replace('DATE', date);

    readRemoteFile(FILE_URL, {
      header: true,
      download: true,
      complete: this.updateDataDailyHandler,
    });
  };

  render() {
    return <MapBase data={this.state.dataTimeseries}></MapBase>;
  }
}

export default App;
