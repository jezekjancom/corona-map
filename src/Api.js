import { readRemoteFile } from 'react-papaparse';

class Api {
  static fetchDataTimeseries = () => {
    readRemoteFile(process.env.REACT_APP_FILE_TIMESERIES, {
      header: true,
      download: true,
      complete: this.updateDataTimeseriesHandler,
    });
  };

  static fetchDataDaily = date => {
    let FILE_URL = process.env.REACT_APP_FILE_DAILY.replace('DATE', date);

    readRemoteFile(FILE_URL, {
      header: true,
      download: true,
      complete: this.updateDataDailyHandler,
    });
  };
}

export default Api;
