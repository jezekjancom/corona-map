import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavLink,
  NavItem,
  NavbarText,
} from 'reactstrap';

const Navigation = props => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Corona Virus Explorer</NavbarBrand>
        <NavbarToggler onClick={toggle} />

        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/overview/">Overview</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/heatmap/">Heatmap</NavLink>
            </NavItem>
          </Nav>
          <NavbarText>
            <a
              href="https://paypal.me/globalnights"
              target="_blank"
              rel="noopener noreferrer"
            >
              DONATE: buy me a coffee...
            </a>
          </NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Navigation;

// <UncontrolledDropdown nav inNavbar>
//               <DropdownToggle nav caret>
//                 Countries
//               </DropdownToggle>
//               <DropdownMenu right>
//                 <DropdownItem key="all" value="ALL">
//                   All
//                 </DropdownItem>
//                 <DropdownItem key="none" value="NONE">
//                   None
//                 </DropdownItem>
//                 {props.countries.map((c, index) => (
//                   <DropdownItem
//                     key={index}
//                     value={c['Country/Region']}
//                     active={props.selectedCountries.includes(
//                       c['Country/Region']
//                     )}
//                   >
//                     {c['name']}
//                   </DropdownItem>
//                 ))}
//                 <DropdownItem divider />
//               </DropdownMenu>
//             </UncontrolledDropdown>
