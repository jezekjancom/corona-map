import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'mapbox-gl/dist/mapbox-gl.css';
import './App.css';
import ReactGA from 'react-ga';
import Api from './Api';
import Navigation from './Nav/Navigation';
import Home from './Home/Home';
import Heatmap from './Heatmap/HeatMap';
import Calculation from './Calculation';
import Overview from './Overview/Overview';

ReactGA.initialize(process.env.REACT_APP_GOOGLE_ANALYTICS_TOKEN);
ReactGA.pageview(window.location.pathname + window.location.search);

class App extends Component {
  state = {
    dataDaily: [],
    dataTimeseries: [],
    selectedCountries: [],
    date: Calculation.actualDate,
    countries: [],
  };

  componentDidMount() {
    Api.fetchDataDaily(this.state.date);
  }

  setCountriesHandler = entries => {
    // let uniqueCountries = [...new Set(entries)];
    var result = Calculation.removeDuplicates(entries, 'Country/Region');
    this.setState({ countries: result });
  };

  render() {
    let countries = this.state.countries;
    return (
      <Router>
        <header className="App">
          <header className="App-header">
            <Navigation
              countries={countries}
              selectedCountries={this.state.selectedCountries}
            ></Navigation>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route path="/heatmap">
                <Heatmap />
              </Route>
              <Route path="/overview">
                <Overview />
              </Route>
            </Switch>
          </header>
        </header>
      </Router>
    );
  }
}

export default App;
