import React from 'react';
import moment from 'moment';

export default class Calculation {
  static actualDate = `${moment()
    .subtract(1, 'days')
    .format('MM-DD-YYYY')}`;

  static sort_by_region = (a, b) => {
    if (a['Country/Region'] < b['Country/Region']) {
      return -1;
    }
    if (a['Country/Region'] > b['Country/Region']) {
      return 1;
    }
    return 0;
  };

  static removeDuplicates = (array, key) => {
    let lookup = new Set();
    return array
      .filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]))
      .sort(Calculation.sort_by_region);
  };

  static sumAndGroupTimeSeriesData = data => {
    const res = {};
    data.forEach(
      ({
        Lat,
        Long,
        'Country/Region': Country,
        'Province/State': City,
        ...dates
      }) => {
        const item = res[Country] || { Country };
        Object.keys(dates).forEach(date => {
          item[date] =
            date in item
              ? Number(item[date]) + Number(dates[date])
              : dates[date];
        });
        res[Country] = { ...item };
      }
    );
    return Object.values(res);
  };

  static getLatestObjectEntryValue(obj) {
    let last = obj[Object.keys(obj)[Object.keys(obj).length - 1]]; // "carrot"
    return last;
  }

  render() {
    return <div></div>;
  }
}
