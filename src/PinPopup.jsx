import React, { PureComponent } from 'react';

export default class PinPopup extends PureComponent {
  render() {
    const { info, last } = this.props;
    const displayName = `${info['Country/Region']}, ${info['Province/State']}: ${last}`;

    return (
      <div>
        <div>{displayName} </div>
      </div>
    );
  }
}
