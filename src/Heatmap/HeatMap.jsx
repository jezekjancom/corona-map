import React, { Component } from 'react';
import { render } from 'react-dom';
import MapGL, { Source, Layer } from 'react-map-gl';
// import { json as requestJson } from 'd3-request';
import { readRemoteFile } from 'react-papaparse';
import ControlPanel from './HeatmapControlPanel';
import { heatmapLayer } from './HeatMapStyle';

function filterFeaturesByDay(featureCollection, time) {
  const date = new Date(parseInt(time));
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();
  const features = featureCollection.features.filter(feature => {
    const featureDate = new Date(parseInt(feature.properties.time));
    return (
      featureDate.getFullYear() === year &&
      featureDate.getMonth() === month &&
      featureDate.getDate() === day
    );
  });

  return { type: 'FeatureCollection', features };
}

export default class HeatMap extends Component {
  constructor(props) {
    super(props);
    const current = new Date().getTime() - 1;
    this.state = {
      viewport: {
        width: window.innerWidth,
        height: window.innerHeight,
        latitude: 29.7577,
        longitude: -9.4376,
        zoom: 2,
        bearing: 0,
        pitch: 0,
      },
      allDay: true,
      startTime: current,
      endTime: current,
      selectedTime: current,
      outbreaks: null,
      max: 0,
    };

    this._handleChangeDay = this._handleChangeDay.bind(this);
    this._handleChangeAllDay = this._handleChangeAllDay.bind(this);
  }

  updateDataTimeseriesHandler = result => {
    const dataTimeseries = result.data;
    this.setState({ dataTimeseries });
    this.remapDataByDate();
  };

  remapDataByDate = () => {
    let max = 0;

    var result = this.state.dataTimeseries.reduce(
      (
        r,
        {
          Lat,
          Long,
          'Country/Region': Country,
          'Province/State': City,
          ...rest
        }
      ) => {
        Object.entries(rest).forEach(([k, v]) => {
          if (parseInt(v) === 0) return;
          max = v > max ? v : max;
          k = new Date(k).getTime();
          r.push({
            type: 'Feature',
            properties: { cases: parseInt(v), time: k },
            geometry: {
              type: 'Point',
              coordinates: [parseFloat(Long), parseFloat(Lat)],
            },
          });
        });
        return r;
      },
      []
    );
    this.setState({ max: max });

    const dates = result.map((key, value) => key.properties.time);
    const first = parseInt(dates[0]);
    const last = parseInt(dates[dates.length - 1]);
    let newState = { type: 'FeatureCollection', features: result };
    this.setState({ data: newState });
    this.setState({ outbreaks: newState });
    this.setState({ startTime: first });
    this.setState({ endTime: last });
    this.setState({ selectedTime: last });
    return result;
  };

  fetchDataTimeseries = () => {
    readRemoteFile(process.env.REACT_APP_FILE_TIMESERIES, {
      header: true,
      download: true,
      complete: this.updateDataTimeseriesHandler,
    });
  };

  componentDidMount() {
    this.fetchDataTimeseries();
  }

  _onViewportChange = viewport => {
    this.setState({ viewport });
  };

  _handleChangeDay = time => {
    this.setState({ selectedTime: time });
    if (this.state.outbreaks) {
      this.setState({ data: filterFeaturesByDay(this.state.outbreaks, time) });
    }
  };

  _renderTooltip() {
    const { hoveredFeature, x, y } = this.state;

    return (
      hoveredFeature && (
        <div className="tooltip" style={{ left: x, top: y }}>
          <div>State: {hoveredFeature.properties.name}</div>
          <div>Median Household Income: {hoveredFeature.properties.value}</div>
          <div>
            Percentile: {(hoveredFeature.properties.percentile / 8) * 100}
          </div>
        </div>
      )
    );
  }

  _handleChangeAllDay = allDay => {
    this.setState({ allDay });
    if (this.state.outbreaks) {
      this.setState({
        data: allDay
          ? this.state.outbreaks
          : filterFeaturesByDay(this.state.outbreaks, this.state.selectedTime),
      });
    }
  };
  render() {
    const {
      viewport,
      data,
      allDay,
      selectedTime,
      startTime,
      max,
      endTime,
    } = this.state;
    return (
      <div>
        <MapGL
          {...viewport}
          mapStyle="mapbox://styles/mapbox/dark-v9"
          onViewportChange={this._onViewportChange}
          mapboxApiAccessToken={process.env.REACT_APP_MAP_TOKEN}
        >
          {data && (
            <Source type="geojson" data={data}>
              <Layer {...heatmapLayer(max)} />
            </Source>
          )}
        </MapGL>
        <ControlPanel
          containerComponent={this.props.containerComponent}
          startTime={startTime}
          endTime={endTime}
          selectedTime={selectedTime}
          allDay={allDay}
          onChangeDay={this._handleChangeDay}
          onChangeAllDay={this._handleChangeAllDay}
        />
      </div>
    );
  }
}

export function renderToDom(container) {
  render(<HeatMap />, container);
}
