import React, { Component } from 'react';
import MapGL, { Popup } from 'react-map-gl';
import Pin from './Pin';
import PinPopup from './PinPopup';
import Calculation from './Calculation';

class MapBase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        width: window.innerWidth,
        height: window.innerHeight,
        latitude: 29.7577,
        longitude: -9.4376,
        zoom: 1,
      },
      popupInfo: null,
    };
  }

  updateViewport = viewport => {
    this.setState({ viewport });
  };

  clickMarkerHandler = info => {
    this.setState({ popupInfo: info });
  };

  renderPopup = () => {
    const { popupInfo } = this.state;
    return (
      this.state.popupInfo && (
        <Popup
          tipSize={5}
          anchor="top"
          longitude={parseFloat(popupInfo['Long'])}
          latitude={parseFloat(popupInfo['Lat'])}
          closeOnClick={false}
          onClose={() => this.setState({ popupInfo: null })}
        >
          <PinPopup
            info={popupInfo}
            last={Calculation.getLatestObjectEntryValue(popupInfo)}
          />
        </Popup>
      )
    );
  };

  render() {
    return (
      <MapGL
        {...this.state.viewport}
        mapStyle="mapbox://styles/mapbox/dark-v9"
        onViewportChange={this.updateViewport}
        mapboxApiAccessToken={process.env.REACT_APP_MAP_TOKEN}
      >
        <Pin data={this.props.data} onClick={this.clickMarkerHandler} />
        {this.renderPopup()}
      </MapGL>
    );
  }
}

export default MapBase;
